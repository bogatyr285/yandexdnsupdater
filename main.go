package main

import (
	"encoding/json"
	"flag"
	"io/ioutil"
	"log"
	"net/http"
	"strconv"
	"strings"
	"time"
	"errors"
)
//По неизвестным причинам возвращеные ошибки(errors.New("some err")) не отображаются в логах, поэтому делаем колхозно. аля log.print() return и пустое поле
var (
	domain        string 
	apiToken      string 
	PathToLogFile string 
)

type yaDnsList struct {
	Domain  string
	Success string
	Records []struct {
		Record_id int
		Type      string
	}
}
func ParseFlag() error {
	flag.StringVar(&domain, "d", domain, "Domain name")
	flag.StringVar(&apiToken, "t", apiToken, "Yandex dns api token")
	flag.StringVar(&PathToLogFile, "p", PathToLogFile, "PathToLogFile")
	flag.Parse()

    if domain=="" || apiToken=="" || PathToLogFile==""{
        return errors.New("Some field empty")
    }
	return nil
}

func main() {
   	if err := ParseFlag(); err != nil {
		log.Fatal(err)
	}	
	if err := InitLogger(); err != nil { //Init logger
		log.Fatal(err)
	}
	curIp := getIp()
	yaChangeIpRequest(curIp)
	for {
		ip := getIp()

		if curIp != ip && ip != "" {
            
			if err := yaChangeIpRequest(ip); err != nil {
				Logger("ChangeIpRequest " + err.Error())
				continue
			} else {
				curIp = ip
			}
		}
		time.Sleep(60 * time.Second)
	}
}

func getIp() (ip string) {
	c := &http.Client{
		Timeout: 10 * time.Second,
	}
	for {
		//Logger("Get ip request")
		resp, err := c.Get(`http://ipinfo.io/ip`)
		if err != nil {
			Logger("Err. get ip " + err.Error())
			time.Sleep(10 * time.Second)
			continue
		}
		defer resp.Body.Close()
		body, _ := ioutil.ReadAll(resp.Body)
		ip = string(body)
		if string(body) != "" {
			break
		}
	}
	return ip
}

func yaChangeIpRequest(ip string) (err error) {
	Logger("Send request to yandex dns. Change ip addr " + ip)

	url := `https://pddimp.yandex.ru/api2/admin/dns/edit`

	recordId := yaGetRecordId()
	if recordId == "" {
        Logger("Err. Get record_id")
		return 
	}
	param := `domain=` + domain + `&record_id=` + recordId + `&content=` + ip
	req, err := http.NewRequest("POST", url, strings.NewReader(param))
	if err != nil {
        Logger("Err. Request  "+ err.Error())
		return
	}
	req.Header.Add("Content-Type", "application/x-www-form-urlencoded")
	req.Header.Add("PddToken", apiToken)
	client := &http.Client{Timeout: 10 * time.Second}
	resp, err := client.Do(req)
	if err != nil {
        Logger("Err. yaChangeIpRequest:"+ err.Error())
		return 
	}
	defer resp.Body.Close()

	body, _ := ioutil.ReadAll(resp.Body)
	Logger("Responce: " + string(body))
	return
}

func yaGetRecordId() (id string) {
	log.Print("Send request to yandex dns. Get A record id ")

	url := `https://pddimp.yandex.ru/api2/admin/dns/list`
	param := `domain=` + domain
	req, err := http.NewRequest("GET", url, strings.NewReader(param))
	if err != nil {
        Logger("Err. Request " + err.Error())
		return 
	}
	req.Header.Add("Content-Type", "application/x-www-form-urlencoded")
	req.Header.Add("PddToken", apiToken)

	client := &http.Client{Timeout: 10 * time.Second}
	resp, err := client.Do(req)
	if err != nil {
        Logger("Err. yaGetRecordId:"+ err.Error())
        return
	}
	defer resp.Body.Close()

	body, _ := ioutil.ReadAll(resp.Body)

	yaAnswer := yaDnsList{}
	err = json.Unmarshal(body, &yaAnswer)
	if err != nil {
        Logger("Err. yaGetRecordId: "+ err.Error())
		return 
	}
	for _, v := range yaAnswer.Records {
		if v.Type == "A" {
			id = strconv.Itoa(v.Record_id)
			Logger("Found " + id)
			break
		}
	}
	return
}
