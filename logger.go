package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"time"
)

var logFileName string = "/log.txt"

func InitLogger() error {
	err := os.MkdirAll(PathToLogFile, os.ModePerm)
	if err != nil {
		return err
	}
	return nil
}

func Logger(msg interface{}) error {
	t := time.Now().Format("2006-01-02 15:04:05")
	log.Printf("%v",msg)
	if _, err := os.Stat(PathToLogFile + logFileName); os.IsNotExist(err) {
		_, err := os.Create(PathToLogFile + logFileName)
		if err != nil {
			log.Printf("err", "Logger", "Cannot create file "+err.Error())
		}
	}
	f, err := os.OpenFile(PathToLogFile+logFileName, os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0644)
	if err != nil {
		log.Printf(`Open log file ` + err.Error())
	}
	defer f.Close()
	w := bufio.NewWriter(f)
	_, err = fmt.Fprintf(w, "[%v] %v\n", t, msg)
	if err != nil {
		log.Printf("Cannot  write log file %v", err.Error())
		return err
	}
	err = w.Flush()
	if err != nil {
		log.Printf("Cannot  save log file %v", err.Error())
		return err
	}
	return nil
}
